/*
 * weather object to store the weather data
 * 
 */


function weatherObject (id, lat, long, temp, humidity){
	 this.id = id;
	 this.lat = lat;
	 this.long = long;
	 this.temp = temp;
	 this.humidity = humidity;

}



function initLoadButton() {

	// Store
	if (localStorage.getItem("weatherSaved") == "1" ) {
		
		console.log('data is saved');
		loadJson();  
		
	} else {
		
		console.log('no storage');
		loadJson();  
	}

}
/*
 * Simple function to map weather model to html
 */
function mapToTemplate(weather) {
	
	$('#weather_id').text(weather.id);
	$('#weather_lat').text(weather.lat);
	$('#weather_long').text(weather.long);
	$('#weather_temp').text(weather.temp);
	$('#weather_humidity').text(weather.humidity);

}



function loadJson() {
    $.getJSON("weather.json",function( data ) {
        $.each( data.list, function( key, val ) {
            
          //  console.log("Loading key [ %s ] with value [ %s ]  Lat: is [ %s ]  Long: is [ %s ] Temp: is [ %s ]   Humidity:  is [ %s ]", key, val.id,val.coord.lat,val.coord.lon,val.main.temp,val.main.humidity);
           var weatherItem = new weatherObject(val.id, val.coord.lat, val.coord.lon, val.main.temp,val.main.humidity);
           mapToTemplate(weatherItem);
           localStorage.setItem("weatherSaved","1");
   
        }); //each 
    
    });
}   //end json function

